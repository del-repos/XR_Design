﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVRTouchSample;
using System;

public class LineDrawing : MonoBehaviour
{
    public GameObject opposite;
    public GameObject Player;
    private LineRenderer lr;
    private Vector3 upPos;
    private Vector3 downPos;
    private float startX;
    private float startY;
    private float startZ;
    private Vector3 midpoint;
    //public GameObject plane;
    //public PortalMesh pm;
    // Start is called before the first frame update
    private int direction = 0; //0 = none, 1 = backwards, 2 = forwards
    private int[] s = { -995, 0, 1030 };
    private int sindx;
    private bool canWarp;

    public GameObject[] aud = new GameObject[3];
    void Start()
    {
        lr = this.GetComponent<LineRenderer>();
        lr.enabled = false;
        //startPos = Vector3.Lerp(this.transform.position, opposite.transform.position, 0.5f);
        Debug.Log(sindx);
        canWarp = false;
        direction = 0;
        sindx = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            startX = Mathf.Abs(this.transform.position.x - opposite.transform.position.x);
            startY = Mathf.Abs(this.transform.position.y - opposite.transform.position.y);
            startZ = Mathf.Abs(this.transform.position.z - opposite.transform.position.z);
            canWarp = true;
            direction = 0;
        }

        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            //pm.enabled = true;
            direction = 1;
            lr.enabled = true;
            lr.startColor = new Color(0, 0, 255);
            lr.endColor = new Color(0, 0, 0);
            midpoint = Vector3.Lerp(this.transform.position, opposite.transform.position, 0.5f);
            upPos = new Vector3(midpoint.x, (midpoint.y + Vector3.Distance(this.transform.position, opposite.transform.position)), midpoint.z);
            downPos = new Vector3(midpoint.x, (midpoint.y - Vector3.Distance(this.transform.position, opposite.transform.position)), midpoint.z);
        }
        else if (OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger))
        {
            //pm.enabled = true;
            direction = 2;
            lr.enabled = true;
            lr.startColor = new Color(255, 0, 0);
            lr.endColor = new Color(0, 0, 0);
            midpoint = Vector3.Lerp(this.transform.position, opposite.transform.position, 0.5f);
            upPos = new Vector3(midpoint.x, (midpoint.y + Vector3.Distance(this.transform.position, opposite.transform.position)), midpoint.z);
            downPos = new Vector3(midpoint.x, (midpoint.y - Vector3.Distance(this.transform.position, opposite.transform.position)), midpoint.z);
        }

        if (startX <= 0.25f && startY <= 0.25f && startZ <= 0.25f)
        {
            lr.SetPosition(0, upPos);
            lr.SetPosition(1, opposite.transform.position);
            lr.SetPosition(2, downPos);
            lr.SetPosition(3, this.transform.position);
            lr.SetPosition(4, upPos);
        }

        if (OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger) && OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            //pm.enabled = false;
            //plane.SetActive(false);
            lr.SetPosition(0, Vector3.zero);
            lr.SetPosition(1, Vector3.zero);
            lr.SetPosition(2, Vector3.zero);
            lr.SetPosition(3, Vector3.zero);
            lr.SetPosition(4, Vector3.zero);
            lr.enabled = false;
            direction = 0;
            canWarp = false;
        }
        else if (!OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger) && !OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            //pm.enabled = false;
            //plane.SetActive(false);
            lr.SetPosition(0, Vector3.zero);
            lr.SetPosition(1, Vector3.zero);
            lr.SetPosition(2, Vector3.zero);
            lr.SetPosition(3, Vector3.zero);
            lr.SetPosition(4, Vector3.zero);
            lr.enabled = false;
            direction = 0;
            canWarp = false;
        }

        if (direction != 0 && canWarp)
        {
            if (Vector3.Distance(this.transform.position, opposite.transform.position) >= 1.0f)
            {
                aud[sindx].SetActive(false);
                if (direction == 1)
                {
                    if (sindx > 0)
                    {
                        sindx--;
                    }
                    else if (sindx == 0)
                    {
                        sindx = 2;
                    }
                }
                else if (direction == 2)
                {
                    if (sindx < 2)
                    {
                        sindx++;
                    }
                    else if (sindx == 2)
                    {
                        sindx = 0;
                    }
                }
                Player.SetActive(false);
                Player.gameObject.transform.position = new Vector3(Player.transform.position.x, s[sindx], Player.transform.position.z);
                aud[sindx].SetActive(true);
                Player.SetActive(true);
                canWarp = false;
            }
        }
    }
}