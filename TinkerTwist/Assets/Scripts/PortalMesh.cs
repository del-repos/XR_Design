﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalMesh : MonoBehaviour
{
    Vector3[] vertices= new Vector3[5];
    Vector2[] uvs;
    int[] newTriangles;
    LineRenderer lr;
    public GameObject plane;
    Mesh mesh;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("start");
        plane.SetActive(true);
        lr = GetComponent<LineRenderer>();

        mesh = new Mesh();
        plane.GetComponent<MeshFilter>().mesh = mesh;
        mesh.Clear();

        vertices[0] = lr.GetPosition(0);
        vertices[1] = lr.GetPosition(1);
        vertices[2] = lr.GetPosition(2);
        vertices[3] = lr.GetPosition(3);
        vertices[4] = lr.GetPosition(4);

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = newTriangles;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("start");
        Debug.Log(vertices[0]);
        plane.SetActive(true);
        lr = GetComponent<LineRenderer>();

        mesh = plane.GetComponent<MeshFilter>().mesh;
        mesh.Clear();

        vertices[0] = lr.GetPosition(0);
        vertices[1] = lr.GetPosition(1);
        vertices[2] = lr.GetPosition(2);
        vertices[3] = lr.GetPosition(3);
        vertices[4] = lr.GetPosition(4);

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = newTriangles;
    }
}
