﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class InvoCSV : MonoBehaviour
{

    public static string PATH = "Assets/CSV/invo.csv";
    public List<string> invo;


    void addInventory(GameObject gmo)
    {
        try
        {
            using(StreamWriter file = new StreamWriter(PATH, true))
            {
                if(invo.Count > 5)
                {
                    Debug.Log(string.Format("Inventory is full, can not add %s", gmo.name));
                } else
                {
                    invo.Add(gmo.name);
                }
            }
        } catch(IOException ex)
        {
            throw new IOException("ERROR MESSAGE: ", ex);
        }
    }
  

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
