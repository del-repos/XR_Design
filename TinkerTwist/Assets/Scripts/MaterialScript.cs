﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialScript : MonoBehaviour
{
    public GameObject objects;
    private ObjectCollection col;
    // Start is called before the first frame update
    void Start()
    {
        col = objects.GetComponent<ObjectCollection>();
        col.noDuplicate = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<MaterialScript>() != null)
        {
            if(col.Craft(this.gameObject, collision.gameObject))
            {
                Destroy(this.gameObject);
                Destroy(collision.gameObject);
            }
        }
    }
}
