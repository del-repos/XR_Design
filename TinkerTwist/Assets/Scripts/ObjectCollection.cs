﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollection : MonoBehaviour
{
    IDictionary<int, string> objectNames = new Dictionary<int, string>();
    public string[] objectRecipies;
    public bool noDuplicate = false;
    // Start is called before the first frame update
    void Start()
    {
        objectNames.Add(0, "Club");
        objectNames.Add(1, "Club");

        objectNames.Add(2, "Spear");
        objectNames.Add(3, "Spear");

        objectNames.Add(4, "Sledge_Hammer");
        objectNames.Add(5, "Sledge_Hammer");

        objectNames.Add(6, "Blast_Hammer");
        objectNames.Add(7, "Blast_Hammer");

        //objectNames.Add(8, "Wand");
        //objectNames.Add(9, "Wand");

        //Materials
        objectNames.Add(10, "Sharp_Rock");
        objectNames.Add(11, "Sharp_Rock");

        objectNames.Add(12, "Unstable_Cell");
        objectNames.Add(13, "Unstable_Cell");

        //Recipies
        objectRecipies = new string[20];
        //Club
        objectRecipies[0] = "Stick Rock";
        objectRecipies[1] = "Rock Stick";
        //Spear
        objectRecipies[2] = "Stick Sharp_Rock";
        objectRecipies[3] = "Sharp_Rock Stick";
        //Sledge
        objectRecipies[4] = "Stick Scrap_Iron";
        objectRecipies[5] = "Scrap_Iron Stick";
        //Blast
        objectRecipies[6] = "Sledge_Hammer Unstable_Cell";
        objectRecipies[7] = "Unstable_Cell Sledge_Hammer";
        //Wand
        //objectRecipies[8] = "Stick Gemstone";
        //objectRecipies[9] = "Gemstone Stick";
        //Sharp Rock
        //objectRecipies[10] = "Rock Rock";
        //objectRecipies[11] = "Rock Rock";
        //Unstable
        objectRecipies[12] = "Fuel_Cell Tesla_Coil";
        objectRecipies[13] = "Tesla_Coil Fuel_Cell";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Craft(GameObject item1, GameObject item2)
    {
        bool succ = false;
        if(noDuplicate)
        {
            string rec = item1.name.Split('(')[0] + " " + item2.name.Split('(')[0];
            int index = System.Array.IndexOf(objectRecipies, rec);

            string result;
            if (objectNames.TryGetValue(index, out result))
            {
                GameObject c = (GameObject)Resources.Load("PreFabs/" + result);
                GameObject cr = Instantiate(c, Vector3.Lerp(item1.transform.position, item2.transform.position, 0.5f), transform.rotation);
                cr.GetComponent<BoxCollider>().enabled = true;
                succ = true;
            }
        }
        noDuplicate = !noDuplicate;
        return succ;
    }
}
