﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ChangeScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        waitTen();
        OpenScene("PastRegion1");
        waitTen();
        OpenScene("FutureRegion1");
          
    }


    IEnumerator waitTen()
    {
       yield return new WaitForSeconds(10.0f);
    }

    public void OpenScene(string scene)
    {
        SceneManager.LoadScene(scene);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
