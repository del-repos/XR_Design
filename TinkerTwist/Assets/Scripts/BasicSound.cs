﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicSound : MonoBehaviour
{
    public AudioSource ac;
    // Start is called before the first frame update
    void Start()
    {
        ac = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        ac.Play();
    }
}
