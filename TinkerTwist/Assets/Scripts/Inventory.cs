﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVRTouchSample;
using System;
using System.IO;
using UnityEngine.SceneManagement;


public class Inventory : MonoBehaviour
{
    public GameObject invCol;
    private bool open;
    private GameObject current;
    public GameObject ball1;
    public GameObject ball2;
    public GameObject ball3;
    public GameObject ball4;
    public GameObject ball5;
    private GameObject t1, t2, t3, t4, t5;
    private bool t1Grabbed, t2Grabbed, t3Grabbed, t4Grabbed, t5Grabbed = false;
    public string[] invC = new string[5];

    //public OVRGrabber LeftGrabber;
    public GameObject RightGrabber;
    private GameObject temp;
    private bool canOpen = false;

    public GameObject objCol;
    // Start is called before the first frame update
    void Start()
    {
        open = false;
        current = null;

        for (int x = 0; x < 5; x++)
        {
            if (invC[x] == null)
            {
                invC[x] = "";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            if (RightGrabber.GetComponent<OVRGrabber>().m_grabbedObj != null)
            {
                temp = RightGrabber.GetComponent<OVRGrabber>().m_grabbedObj.gameObject;

                if (setInv(temp.name))
                {
                    RightGrabber.GetComponent<OVRGrabber>().GrabEnd();
                    Destroy(temp.gameObject);
                }
            }

            if (!(this.gameObject.transform.eulerAngles.z < 300 && this.gameObject.transform.eulerAngles.z > 240))
            {
                canOpen = true;
            }
        }

        if (OVRInput.Get(OVRInput.Button.One))
        {
            if (this.gameObject.transform.eulerAngles.z < 300 && this.gameObject.transform.eulerAngles.z > 240 && canOpen)
            {
                if (!open)
                {
                    current = Instantiate(invCol, this.gameObject.transform.position, Quaternion.Euler(this.gameObject.transform.eulerAngles.x, this.gameObject.transform.eulerAngles.y + 90, this.gameObject.transform.eulerAngles.z + 90));
                    for (int x = 0; x < invC.Length; x++)
                    {
                        if (x + 1 == 1)
                        {
                            if (invC[x].Equals(""))
                            {
                                ball1 = current.transform.Find("Sphere").gameObject;
                            }
                            else
                            {
                                ball1 = (GameObject)Resources.Load("PreFabs/" + invC[x].Split('(')[0]);
                                ball1.GetComponent<InvMovement>().pos = (current.transform.Find("GameObject")).gameObject;
                                t1 = Instantiate(ball1, current.transform.position, current.transform.rotation);
                                t1.transform.localScale = new Vector3(1f, 1f, 1f);
                                t1.GetComponent<InvMovement>().enabled = true;
                                t1.GetComponent<InvMovement>().pos = ball1.GetComponent<InvMovement>().pos;
                            }
                        }
                        else if (x + 1 == 2)
                        {
                            if (invC[x].Equals(""))
                            {
                                ball2 = current.transform.Find("Sphere (1)").gameObject;
                            }
                            else
                            {
                                ball2 = (GameObject)Resources.Load("PreFabs/" + invC[x].Split('(')[0]);
                                ball2.GetComponent<InvMovement>().pos = (current.transform.Find("GameObject (1)")).gameObject;
                                t2 = Instantiate(ball2, current.transform.position, current.transform.rotation);
                                t2.transform.localScale = new Vector3(1f, 1f, 1f);
                                t2.GetComponent<InvMovement>().enabled = true;
                                t2.GetComponent<InvMovement>().pos = ball2.GetComponent<InvMovement>().pos;
                            }
                        }
                        else if (x + 1 == 3)
                        {
                            if (invC[x].Equals(""))
                            {
                                ball3 = current.transform.Find("Sphere (2)").gameObject;
                            }
                            else
                            {
                                ball3 = (GameObject)Resources.Load("PreFabs/" + invC[x].Split('(')[0]);
                                ball3.GetComponent<InvMovement>().pos = (current.transform.Find("GameObject (2)")).gameObject;
                                t3 = Instantiate(ball3, current.transform.position, current.transform.rotation);
                                t3.transform.localScale = new Vector3(1f, 1f, 1f);
                                t3.GetComponent<InvMovement>().enabled = true;
                                t3.GetComponent<InvMovement>().pos = ball3.GetComponent<InvMovement>().pos;
                            }
                        }
                        else if (x + 1 == 4)
                        {
                            if (invC[x].Equals(""))
                            {
                                ball4 = current.transform.Find("Sphere (3)").gameObject;
                            }
                            else
                            {
                                ball4 = (GameObject)Resources.Load("PreFabs/" + invC[x].Split('(')[0]);
                                ball4.GetComponent<InvMovement>().pos = (current.transform.Find("GameObject (3)")).gameObject;
                                t4 = Instantiate(ball4, current.transform.position, current.transform.rotation);
                                t4.transform.localScale = new Vector3(1f, 1f, 1f);
                                t4.GetComponent<InvMovement>().enabled = true;
                                t4.GetComponent<InvMovement>().pos = ball4.GetComponent<InvMovement>().pos;
                            }
                        }
                        else if (x + 1 == 5)
                        {
                            if (invC[x].Equals(""))
                            {
                                ball5 = current.transform.Find("Sphere (4)").gameObject;
                            }
                            else
                            {
                                ball5 = (GameObject)Resources.Load("PreFabs/" + invC[x].Split('(')[0]);
                                ball5.GetComponent<InvMovement>().pos = (current.transform.Find("GameObject (4)")).gameObject;
                                t5 = Instantiate(ball5, current.transform.position, current.transform.rotation);
                                t5      .transform.localScale = new Vector3(1f, 1f, 1f);
                                t5.GetComponent<InvMovement>().enabled = true;
                                t5.GetComponent<InvMovement>().pos = ball5.GetComponent<InvMovement>().pos;
                            }
                        }
                    }
                    open = true;
                    canOpen = false;
                }
            }
        }

        if (OVRInput.GetUp(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger) || RightGrabber.GetComponent<OVRGrabber>().grabbedObject != null)
        {
            if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject != null)
            {
                if (t1 && !(RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t1)))
                {
                    Destroy(t1);
                }
                else if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t1))
                {
                    invC[0] = "";
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<BoxCollider>().enabled = true;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<InvMovement>().enabled = false;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.AddComponent<MaterialScript>();
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<MaterialScript>().objects = objCol;
                    t1 = null;
                }
                if (t2 && !(RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t2)))
                {
                    Destroy(t2);
                }
                else if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t2))
                {
                    invC[1] = "";
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<BoxCollider>().enabled = true;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<InvMovement>().enabled = false;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.AddComponent<MaterialScript>();
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<MaterialScript>().objects = objCol;
                    t2 = null;
                }
                if (t3 && !(RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t3)))
                {
                    Destroy(t3);
                }
                else if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t3))
                {
                    invC[2] = "";
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<BoxCollider>().enabled = true;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<InvMovement>().enabled = false;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.AddComponent<MaterialScript>();
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<MaterialScript>().objects = objCol;
                    t3 = null;
                }
                if (t4 && !(RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t4)))
                {
                    Destroy(t4);
                }
                else if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t4))
                {
                    invC[3] = "";
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<BoxCollider>().enabled = true;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<InvMovement>().enabled = false;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.AddComponent<MaterialScript>();
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<MaterialScript>().objects = objCol;
                    t4 = null;
                }
                if (t5 && !(RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t5)))
                {
                    Destroy(t5);
                }
                else if (RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.Equals(t5))
                {
                    invC[4] = "";
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.transform.localScale = new Vector3(10.0f, 10.0f, 10.0f);
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<BoxCollider>().enabled = true;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<InvMovement>().enabled = false;
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.AddComponent<MaterialScript>();
                    RightGrabber.GetComponent<OVRGrabber>().grabbedObject.gameObject.GetComponent<MaterialScript>().objects = objCol;
                    t5 = null;
                }
            }
            else
            {
                if (current && ball1 && ball2 && ball3 && ball4 && ball5)
                {
                    Destroy(current);
                }
            }

            if (current && ball1 && ball2 && ball3 && ball4 && ball5)
            {
                Destroy(current);
            }
            open = false;
        }
    }

    public bool setInv(string itemName)
    {
        int x;
        for (x = 0; x < 5; x++)
        {
            if (invC[x].Equals(""))
            {
                break;
            }
        }

        if (x < 5)
        {
            invC[x] = itemName;
            return true;
        }
        else if (x >= 5)
        {
            return false;
        }

        return false;
    }
}
