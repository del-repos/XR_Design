﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvMovement : MonoBehaviour
{
    public GameObject pos;
    private Vector3 target;
    private float step;
    public float speed = 1.0f;
    private bool done;
    // Start is called before the first frame update
    void Start()
    {
        target = pos.transform.position;
        step = speed * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, target) < 0.001f)
        {
            done = true;
        }
        if (!done)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, step);
        }
    }
}
