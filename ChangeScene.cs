using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeScene : MonoBehaviour {


    void SwitchToScene(string scene_name)
    {
        SceneManager.LoadScene(scene_name);
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // we need to specific the input on oculus and switch key input
		if(Input.GetKeyDown(KeyCode.Space))
        {
            // conditionals based upon where the user wants to go and perhaps even a switch statement.
            SwitchToScene("");
        }
	}
}
